class PriorityLane:
	def __init__(self):
		self.list = []
	def enqueue(self, item):
		self.list.append(item)
	def dequeue(self):
		print(f'{self.list[-1]}')
		self.list.pop()
	def is_empty(self):
		if(len(self.list)==0):
			print(True)
		else:
			print(False)
	def peek(self):
		print(self.list[-1])
	def size(self):
		return len(self.list)
	def print(self):
		print(self.list)

security = PriorityLane()
security.enqueue("normal person")
security.enqueue("PWD")
security.enqueue("Pregnant")
security.enqueue("senior")
print("Original queue list items:")
security.print()
print("Is the security list empty")
security.is_empty()
print(f"Current size of the security list:")
security.size()
print(f"Peek of the new list queue item :")
security.peek()
print(f"The item is removed from the security list:")
security.dequeue()


